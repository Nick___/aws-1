import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Footer from "./components/Footer";
import Header from "./components/Header";
import { DarkModeContext, Theme, themeList, ThemeName } from "./store/context";
import AppContainer from "./components/styled/AppContainer";

import loadable from "@loadable/component";
import MainPageLoader from "./views/MainPageLoader";

const Loading = () => <h1>Loading</h1>;

const LoadableMoviePage = loadable(() => import("./views/MoviePage"), {
  fallback: <Loading />,
});

const LoadableMainPage = loadable(() => import("./views/MainPage"), {
  fallback: <MainPageLoader />, // work in progress
});

export default function App() {
  const [activeTheme, setActiveTheme] = useState(themeList.light);

  return (
    <DarkModeContext.Provider
      value={{
        theme: activeTheme,
        setTheme: (themeKey: ThemeName) => {
          setActiveTheme(themeList[themeKey] as Theme);
        },
      }}
    >
      <AppContainer>
        <Header></Header>
        <Router>
          <Routes>
            <Route path="/" element={<LoadableMainPage />} />
            <Route path="/movie/:id" element={<LoadableMoviePage />}></Route>
          </Routes>
        </Router>
        <Footer></Footer>
      </AppContainer>
    </DarkModeContext.Provider>
  );
}
