import React, { memo } from "react";
import { ApiError, Movie } from "../utils/typesApi";
import MovieCardLoader from "./MovieCardLoader";
import SimpleMovieCard from "./SimpleMovieCard";
import { MovieSliderContainer } from "./styled";
import SectionHeading from "./styled/SectionHeading";

interface MovieListCardDisplayProps {
  movieList: Movie[] | null | undefined;
  error: ApiError | null | undefined;
  headingText: string;
}

const placeholderList: number[] = [];
for (let i = 0; i++; i < 20) {
  placeholderList.push(i);
}

function MovieListCardDisplay({
  movieList,
  error,
  headingText,
}: MovieListCardDisplayProps) {
  return (
    <div>
      <SectionHeading>{headingText}</SectionHeading>
      <MovieSliderContainer>
        {!error &&
          movieList?.map((mov) => (
            <SimpleMovieCard movieData={mov} key={mov.id} />
          ))}
      </MovieSliderContainer>
      <p>{error?.message}</p>
    </div>
  );
}

export default memo(MovieListCardDisplay);
