import React, { memo } from "react";
import SimpleMovieCardLoader from "./SimpleMovieCardLoader";
import { MovieSliderContainer } from "./styled";
import SectionHeading from "./styled/SectionHeading";

interface MovieListCardDisplayProps {
  headingText: string;
}

function MovieListCardListDisplayLoader({
  headingText,
}: MovieListCardDisplayProps) {
  const placeholderList: number[] = [];
  for (let i = 0; i < 20; i++) {
    placeholderList.push(i);
  }
  return (
    <div>
      <SectionHeading>{headingText}</SectionHeading>
      <MovieSliderContainer>
        {placeholderList.map((id) => (
          <SimpleMovieCardLoader key={id}></SimpleMovieCardLoader>
        ))}
      </MovieSliderContainer>
    </div>
  );
}

export default memo(MovieListCardListDisplayLoader);
