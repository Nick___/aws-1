import React from "react";
import styled from "styled-components";
import MovieCardLoader from "./MovieCardLoader";

export default function MovieListLoader() {
  const placeholderList: number[] = [];
  for (let i = 0; i < 20; i++) {
    placeholderList.push(i);
  }
  return (
    <MovieListContainer>
      <MovieCardListWrapper>
        {placeholderList.map((id: number) => {
          return <MovieCardLoader key={id} />;
        })}
      </MovieCardListWrapper>
    </MovieListContainer>
  );
}

const MovieCardListWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
`;

const MovieListContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
