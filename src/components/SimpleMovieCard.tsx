import React, { memo, useCallback, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import movieApiClient from "../utils/movieApiClient";
import { Movie } from "../utils/typesApi";

function SimpleMovieCard({ movieData }: { movieData: Movie }) {
  const navigate = useNavigate();

  const onCardClick = useCallback(() => {
      navigate(`/movie/${movieData.id}`);
  }, [movieData.id, navigate]);

  const imgSrc = useMemo(() => {
    return movieApiClient.buildMoviePosterUrl(movieData.poster_path, true);
  }, [movieData.poster_path]);

  return (
    <SimpleMovieCardContainer>
      <SimpleMovieCardImage
        src={imgSrc}
        height="174"
        width="115"
        onClick={onCardClick}
      ></SimpleMovieCardImage>
    </SimpleMovieCardContainer>
  );
}

// HOC wrapping the original component
export default memo(SimpleMovieCard);

const SimpleMovieCardContainer = styled.div`
  margin-left: 2px;
  margin-right: 2px;
`;

const SimpleMovieCardImage = styled.img`
  &:hover {
    cursor: pointer;
  }
`;
