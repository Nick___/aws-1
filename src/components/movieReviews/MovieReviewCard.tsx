import React, { useMemo } from "react";
import { MovieReview } from "../../utils/typesApi";

export default function MovieReviewCard({ review }: { review: MovieReview }) {
  const createdAtDate = useMemo(() => {
    return Intl.DateTimeFormat().format(new Date(review.created_at));
  }, [review.created_at]);
  return (
    <div>
      <p>
        Review by{" "}
        <strong data-testid="review-card-username">
          {review.author_details.username}
        </strong>
      </p>
      <p data-testid="review-card-content">{review.content}</p>
      <p data-testid="review-card-date">Date: {createdAtDate}</p>
    </div>
  );
}
