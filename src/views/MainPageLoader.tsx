import React from "react";
import ContentLoader from "react-content-loader";
import { PageContainer } from "../components/styled";

const SearchBarLoader = () => (
  <ContentLoader
    speed={2}
    width={"100%"}
    height={300}
    viewBox="0 0 400 160"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
  >
    <rect x="0" y="88" rx="3" ry="3" width="178" height="6" />
  </ContentLoader>
);

const MovieListLoader = () => (
  <ContentLoader
    speed={2}
    width={"100%"}
    height={"20px"}
    viewBox="0 0 400 160"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
  >
    <rect x="48" y="8" rx="3" ry="3" width="88" height="6" />
  </ContentLoader>
);

const PaginationLoader = () => (
  <ContentLoader
    speed={2}
    width={"100%"}
    height={50}
    viewBox="0 0 400 160"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
  ></ContentLoader>
);

const TrendingNowLoader = () => (
  <ContentLoader
    speed={2}
    width={"calc(50% - 20px)"}
    height={240}
    viewBox="0 0 400 160"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
  >
    <rect x="48" y="8" rx="3" ry="3" width="88" height="6" />
    <rect x="48" y="26" rx="3" ry="3" width="52" height="6" />
    <rect x="0" y="56" rx="3" ry="3" width="410" height="6" />
    <rect x="0" y="72" rx="3" ry="3" width="380" height="6" />
    <rect x="0" y="88" rx="3" ry="3" width="178" height="6" />
  </ContentLoader>
);

const MainPageLoader = () => (
  <PageContainer>
    <SearchBarLoader />
    <MovieListLoader />
    <PaginationLoader />
    <TrendingNowLoader />
    <TrendingNowLoader />
  </PageContainer>
);

export default MainPageLoader;
